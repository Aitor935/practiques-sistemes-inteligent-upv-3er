
package puzzle15;


public class HeuristicaManhattan extends Heuristica {
   	
    @Override
    public int getH(Estado e) {
        int cont = 0;
        
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                
                int ficha = e.getFicha(i, j);
                if (ficha != 0 && ficha != j + i * 4);
                    cont += contadorPaso(ficha, i, j);
            }
        }
        return cont;
    }
    
    public int contadorPaso(int ficha, int i, int j ){
        
        int fi = ficha / 4;
        int fj = ficha % 4;
        
        if(fi > i) fi = fi -i;
        else if(fi < i) fi = i - fi;
        else fi = 0;
        
        if(fj > j) fj = fj -j;
        else if(fj < j) fj = j - fj;
        else fj = 0;
        
        return fi + fj;
    }
}
