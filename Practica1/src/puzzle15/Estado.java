package puzzle15;

import java.util.Arrays;
/**
 * Estado del 15-puzzle.
 */
public class Estado implements Comparable<Estado>
{
/**
 * Construye un estado a partir de un array de fichas.
 * @param f Lista de las 16 fichas (15 y el hueco) del puzzle.
 */
    private int fichas[];
    private Estado padre;
    private int profundidad;
    
public Estado(int...f)
{
    if(f.length != 16)
        throw new UnsupportedOperationException("El tamaño del array debe ser 15.");
    
    int pruebaEstado[] = new int[16];
    
    for(int i = 0; i < 16; i++) {
        
        pruebaEstado[f[i]]++;
    }
    
    for(int j = 0; j < 16; j++) {
        
        if(pruebaEstado[j] != 1) 
            throw new UnsupportedOperationException("puzzle mal definido.");
    }
    
    fichas = new int[16];
    System.arraycopy(f, 0, fichas, 0, 16);
}


/**
 * Constructor de Estado a partir de un Estado padre y un array vacío
 * @param e
 * @param f 
 */
private Estado(Estado e, int[] f)
{
    padre = e;
    profundidad = e.profundidad + 1;
    fichas = f;
}

/**
 * Devuelve el estado padre.
 * @return estado padre
 */
public Estado getPadre()
{
    return padre;
}

/**
 * Devuelve la profundidad del estado.
 * @return profundidad
 */
public int getProfundidad()
{   
    return profundidad;
}


/**
 * Devuelve la ficha que este en la posicion (i,j)
 * @param i
 * @param j
 * @return 
 */
public int getFicha(int i, int j)
{
    
    int ficha = i * 4 + j;
    if(0 >= ficha && ficha > 16)
        throw new IndexOutOfBoundsException();

    return fichas[ficha];
}


/**
 * Devuelve la posición del hueco
 * @return Posición Hueco
 */
public int getHueco() {
    
    int hueco = -1;
    boolean esta = false;
    
    do{        
        hueco++; 
        if(fichas[hueco] == 0)
            esta = true; 
        
    }while(!esta && hueco < 16);
    
    if(0 >= hueco && hueco > 16)
        throw new IndexOutOfBoundsException();
    
    return hueco;
}


/**
 * Mueve el hueco una posición Arriba
 * @return Nuevo Estado modificado
 */
public Estado moverArriba()
{
    
    int limite = getHueco();
    
    if(3 < limite && limite < 16 )
    {    
        int array[] = new int[16];
        System.arraycopy(fichas, 0, array, 0, 16);
        array[limite] = fichas[limite - 4];
        array[limite - 4] = 0;
        return new Estado(this, array);
    } 
    else return null;  
}


/**
 * Mueve el hueco una posición Abajo
 * @return Nuevo Estado modificado
 */
public Estado moverAbajo()
{
    
    int limite = getHueco();
    
    if(0 <= limite && limite < 12 )
    {    
        int array[] = new int[16];
        System.arraycopy(fichas, 0, array, 0, 16);
        array[limite] = fichas[limite + 4];
        array[limite + 4] = 0;
        return new Estado(this, array);
    } 
    else return null;  
}


/**
 * Mueve el hueco una posición a la Izquierda
 * @return Nuevo Estado modificado
 */
public Estado moverIzquierda() {
    
    int hueco = getHueco();
    int limite = hueco % 4;
    
    if(0 < limite && limite < 4 )
    {    
        int array[] = new int[16];
        System.arraycopy(fichas, 0, array, 0, 16);
        array[hueco] = fichas[hueco -1];
        array[hueco -1] = 0;
        return new Estado(this, array);
    } 
    else return null;  
}


/**
 * Mueve el hueco una posición a la Derecha
 * @return Nuevo Estado modificado
 */
public Estado moverDerecha() {
    
    int hueco = getHueco();
    int limite = hueco % 4;
    
    if(0 <= limite && limite < 3 )
    {    
        int array[] = new int[16];
        System.arraycopy(fichas, 0, array, 0, 16);
        array[hueco] = fichas[hueco + 1];
        array[hueco + 1] = 0;
        return new Estado(this, array);
    } 
    else return null;
}

/**
 * Calcula el código hash.
 * @return Código hash.
 */
@Override public int hashCode()
{
    return Arrays.hashCode(fichas);
}

/**
 * Compara un estado con otro objeto.
 * @param obj Objeto a comparar.
 * @return {@code true} si el estado es igual al objeto.
 */
@Override public boolean equals(Object obj)
{
    
    if(obj != null || getClass() == obj.getClass())
       if(comparaFichas(((Estado)obj).fichas)) 
           return true;
    
    return false;
}

/**
 * Compara un estado con otro.
 * Esta comparación es necesaria para utilizar la cola de prioridad.
 * @param e estado
 * @return un número negativo, cero, o positivo si este estado
 *         es menor, igual, o mayor que el estado indicado.
 */
@Override public int compareTo(Estado e)
{
    
    for(int i = 0; i < 16 ; i++)
    {
        if(fichas[i] < e.fichas[i]) return -1;
        else if(fichas[i] > e.fichas[i]) return 1;
    }
    return 0;
}


/**
 * Compara dos arrays de fichas
 * @param f
 * @return False si son diferentes, True si son iguales
 */
public boolean comparaFichas(int[] f) {
    
    for(int i = 0; i < 16; i++)
    {
        if(fichas[i] != f[i])
            return false;
    }
    return true;
}

/**
 * Comprueba si el estado es el objetivo.
 * @return {@code true} si el estado es el objetivo.
 */
public boolean esObjetivo()
{
    for(int i = 0; i < 16; i++)
    {
        if(fichas[i] != i)
            return false;
    }
    return true;
}


/**
 * Método para imprimir las soluciones
 */
public void printSolucion()
{
    if(padre != null)
        padre.printSolucion();

    System.out.println(toString());
}

@Override public String toString() {
    
    StringBuilder sb = new StringBuilder();
    
    for(int i = 0; i < 4; i++)
    {
        for(int j = 0; j < 4; j++)
        {
            int f = getFicha(i, j);
            sb.append(' ');
            
            if(f == 0) sb.append("  ");
            else if(0 < f && f < 10) sb.append(" "+f);
            else sb.append(f);
        }

        sb.append('\n');
    }
    return sb.toString();
    
}
} // Estado