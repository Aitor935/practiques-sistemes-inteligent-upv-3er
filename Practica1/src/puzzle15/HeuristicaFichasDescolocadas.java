
package puzzle15;



public class HeuristicaFichasDescolocadas extends Heuristica {
    
    @Override
    public int getH(Estado e) {
        int cont = 0;
        
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                
                int ficha = e.getFicha(i, j);
                if (ficha != 0 && ficha != j + i * 4);
                    cont ++;
            }
        }
        return cont;
    }
    
}
