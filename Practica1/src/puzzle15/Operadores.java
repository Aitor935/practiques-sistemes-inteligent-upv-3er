package puzzle15;

import lib.*;

//************************************************************************
public enum Operadores implements Operador<Estado>
{
    moverDerecha {@Override public Estado run(Estado e)
    {
        return e.moverDerecha();
    }},

    moverIzquierda {@Override public Estado run(Estado e)
    {
        return e.moverIzquierda();
    }},

    moverArriba {@Override public Estado run(Estado e)
    {
        return e.moverArriba();
    }},

    moverAbajo {@Override public Estado run(Estado e)
    {
        return e.moverAbajo();
    }},

    
} // Operadores
