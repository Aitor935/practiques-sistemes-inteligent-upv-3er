package jarras;

import java.util.*;
import lib.*;

public class Main
{
private static Estado anchura()
{
    HashSet<Estado> repetidos = new HashSet<>();
    LinkedList<Estado> abiertos = new LinkedList<>();
    abiertos.add(new Estado());

    while(!abiertos.isEmpty())
    {
        Estado estado = abiertos.removeFirst();

        if(estado.esObjetivo())
            return estado;

        for(Operador<Estado> o : Operadores.values())
        {
            Estado sucesor = o.run(estado);

            if(sucesor != null && repetidos.add(sucesor))
                abiertos.add(sucesor);
        }
    }

    return null;
}

public static void main(String args[])
{
    anchura().printSolucion();
}

} // Main
