package jarras;

import lib.*;

//************************************************************************
public enum Operadores implements Operador<Estado>
{
    llenarA {@Override public Estado run(Estado e)
    {
        return e.llenarA();
    }},

    llenarB {@Override public Estado run(Estado e)
    {
        return e.llenarB();
    }},

    vaciarA {@Override public Estado run(Estado e)
    {
        return e.vaciarA();
    }},

    vaciarB {@Override public Estado run(Estado e)
    {
        return e.vaciarB();
    }},

    traspAB {@Override public Estado run(Estado e)
    {
        return e.traspAB();
    }},

    traspBA {@Override public Estado run(Estado e)
    {
        return e.traspBA();
    }}
} // Operadores
