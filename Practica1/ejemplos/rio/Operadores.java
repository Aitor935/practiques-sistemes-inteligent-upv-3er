package rio;

import lib.*;

//************************************************************************
public enum Operadores implements Operador<Estado>
{
    viajarOveja {@Override public Estado run(Estado e)
    {
        return e.viajarOveja();
    }},

    viajarCol {@Override public Estado run(Estado e)
    {
        return e.viajarCol();
    }},

    viajarSolo {@Override public Estado run(Estado e)
    {
        return e.viajarSolo();
    }},

    viajarLobo {@Override public Estado run(Estado e)
    {
        return e.viajarLobo();
    }}
}
