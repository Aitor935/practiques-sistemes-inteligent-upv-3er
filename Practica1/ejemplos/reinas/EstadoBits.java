package reinas;

/**
 * Estado de las 8 reinas.
 */
public class EstadoBits
{
private int estado;

/**
 * Indica cuántas reinas hay en el tablero.
 * @return Número de reinas en el tablero.
 */
public int getNumReinas()
{
    return estado >>> 24;
}

/**
 * Crea el estado sucesor i-ésimo, que consiste en añadir una reina
 * en la fila i de la siguiente columna.
 * @param i Fila donde se añadirá la reina.
 * @return EstadoBits sucesor.
 */
public EstadoBits sucesor(int i)
{
    assert i >= 0 && i < 8;
    int j = getNumReinas();
    assert j >= 0 && j < 8;
    EstadoBits e = new EstadoBits();
    e.estado = estado + 0x01000000;
    e.estado |= i << (j*3);
    return e.esValido() ? e : null;
}

/**
 * Indica si en la fila {@code i}, columna {@code j} hay una reina.
 * Si las coordenadas están fuera del tablero, devuelve false.
 * @param i Fila.
 * @param j Columna.
 * @return true si hay una reina en la casilla indicada.
 */
public boolean getReina(int i, int j)
{
    if(i < 0 || i > 7 || j < 0 || j > 7 || j >= getNumReinas())
        return false;
    else
        return i == ((estado >>> (j*3)) & 0x07);
}

/**
 * Indica si el estado es objetivo, es decir, si hay 8 reinas y
 * no se matan entre ellas.
 * @return true si es un estado objetivo.
 */
public boolean esObjetivo()
{
    return getNumReinas() == 8 && esValido();
}

/**
 * Indica si en el tablero no se mata ninguna reina.
 * @return true si no se mata ninguna reina.
 */
public boolean esValido()
{
    for(int i = 0; i < 8; i++)
    {
        boolean horizontal = false,
                diagAsc1   = false,
                diagAsc2   = false,
                diagDes1   = false,
                diagDes2   = false;

        for(int j = 0; j < 8; j++)
        {
            if(getReina(i, j))
            {
                if(horizontal)
                    return false; //................................RETURN
                else
                    horizontal = true;
            }

            if(getReina(i+j, j))
            {
                if(diagDes1)
                    return false; //................................RETURN
                else
                    diagDes1 = true;
            }

            if(getReina(j, i+j))
            {
                if(diagDes2)
                    return false; //................................RETURN
                else
                    diagDes2 = true;
            }

            if(getReina(i-j, j))
            {
                if(diagAsc1)
                    return false; //................................RETURN
                else
                    diagAsc1 = true;
            }

            if(getReina(7-i+j, 7-j))
            {
                if(diagAsc2)
                    return false; //................................RETURN
                else
                    diagAsc2 = true;
            }
        }
    }

    return true; //.................................................RETURN
}

/**
 * Convierte el estado actual a una cadena que representa el tablero.
 * @return Tablero representado como una cadena.
 */
@Override public String toString()
{
    StringBuilder sb = new StringBuilder();

    for(int i = 0; i < 8; i++)
    {
        for(int j = 0; j < 8; j++)
            sb.append(getReina(i, j) ? " X" : " ·");

        sb.append('\n');
    }

    return sb.toString();
}

} // EstadoBits
