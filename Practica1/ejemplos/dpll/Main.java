package dpll;

//************************************************************************
public class Main
{
//------------------------------------------------------------------------
public static void main(String args[])
{
    Modelo modelo = new Modelo();
    //Sentencia sentencia = new Sentencia("A,!B", "!B,!C", "C,A");

    /*
    // Reglas de la página 28 (mundo de wumpus)
    Sentencia sentencia = new Sentencia(
        "!H11",
        "!B11,H12,H21", "!H12,B11", "!H21,B11",
        "!B21,H11,H22,H31", "!H11,B21", "!H22,B21", "!H31,B21",
        "!B11",
        "B21");
    */

    // Reglas del ejercicio 6.
    Sentencia sentencia = new Sentencia(
        "!A,B,E", "!B,A", "!E,A",
        "!E,D",
        "!C,!F,!B",
        "!E,B",
        "!B,F",
        "!B,C");

    System.out.println("Sentencia: "+ sentencia);

    boolean satisfacible = DPLL.run(sentencia, modelo);
    System.out.println("\nSatisfacible: "+ satisfacible);

    if(satisfacible)
        System.out.println("      Modelo: "+ modelo);
}

} // Main
