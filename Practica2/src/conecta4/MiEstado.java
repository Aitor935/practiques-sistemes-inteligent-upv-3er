package conecta4;

/**
 * Estado del juego conecta 4.
 */
public class MiEstado extends Estado {

    public Boolean ficha[][];
    private int miniMAX = 0;

    /**
     * Crea un estado vacío, es decir, sin fichas en el tablero.
     */
    public MiEstado() {
        ficha = new Boolean[DIMX][DIMY];
    }

    /**
     * Crea un estado idéntico a otro.
     *
     * @param e Estado a copiar.
     */
    public MiEstado(Estado e) {
        this();
        for (int i = 0; i < DIMX; i++) {
            for (int j = 0; j < DIMY; j++) {
                ficha[i][j] = e.getFicha(i, j);
            }
        }

    }

    /**
     * @param t
     */
    public MiEstado(Boolean[][] t) {

        ficha = t;

    }

    /**
     * Get de la variable miniMAX
     */
    public int getMinimax() {

        return miniMAX;
    }

    /**
     * Set de la variable miniMAX
     */
    public void setMinimax(int m) {

        miniMAX = m;
    }

    /**
     * Calcula un sucesor poniendo una ficha en la columna x.
     *
     * @param x columna
     * @param max ficha: true para MAX, false para MIN.
     * @return Estado sucesor o null si no caben más fichas en la columna x.
     */
    @Override
    public MiEstado getSucesor(int x, boolean max) {

        int fichas = getFichas(x);
        if (fichas < 6) {

            Boolean[][] aux = new Boolean[DIMX][DIMY];
            for (int i = 0; i < DIMX; i++) {

                System.arraycopy(ficha[i], 0, aux[i], 0, 6);
            }
            aux[x][fichas] = max;
            return new MiEstado(aux);
        }
        return null;

    }

    /**
     * Indica la ficha en la columna x, posición y.
     *
     * @param x columna
     * @param y posición de la ficha en la columna. La posición del fondo es 0.
     * @return true, false o null si hay una ficha MAX, MIN, o ninguna.
     */
    @Override
    public Boolean getFicha(int x, int y) {

        return ficha[x][y];

    }

    /**
     * Indica cuántas fichas hay en la columna x.
     *
     * @param x columna
     * @return Número de fichas en la columna x.
     */
    @Override
    public int getFichas(int x) {
        int numfichas = 0;
        for (int i = 0; i < 6; i++) {
            if (ficha[x][i] != null) {
                numfichas++;
            }
        }
        return numfichas;
    }

    /**
     * Calcula el valor de utilidad de un estado.
     *
     * @return Valor de utilidad. GANADOR si gana MAX. -GANADOR si gana MIN.
     */
    @Override
    public int getUtilidad() {

        int contador = horizontal();
        if (contador == 1000) {
            return 1000;
        } else if (contador == -1000) {
            return -1000;
        }

        contador += vertical();
        if (contador == 1000) {
            return 1000;
        } else if (contador == -1000) {
            return -1000;
        }

        contador += diagonalDerecha();
        if (contador == 1000) {
            return 1000;
        } else if (contador == -1000) {
            return -1000;
        }

        contador += diagonalIzquierda();
        if (contador == 1000) {
            return 1000;
        } else if (contador == -1000) {
            return -1000;
        }

        return contador;
    }

    /**
     * Devuelve el número de cuatros en raya que encuentra en las horizontales
     *
     * @return int contador
     */
    public int horizontal() {

        int contador = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < DIMY; j++) {

                int max = 0;
                int min = 0;
                int blanco = 0;
                for (int k = 0; k < 4; k++) {

                    if (ficha[i + k][j] == null) {
                        blanco++;
                    } else if (ficha[i + k][j]) {
                        max++;
                    } else {
                        min++;
                    }
                }
                if (max == 4) {
                    return 1000;
                } else if (min == 4) {
                    return -1000;
                }

                if (max + blanco == 4) {
                    contador++;
                }
                if (min + blanco == 4) {
                    contador--;
                }
            }
        }
        return contador;
    }

    /**
     * Devuelve el número de cuatros en raya que encuentra en las verticales
     *
     * @return int contador
     */
    public int vertical() {

        int contador = 0;
        for (int i = 0; i < DIMX; i++) {
            for (int j = 0; j < 3; j++) {

                int max = 0;
                int min = 0;
                int blanco = 0;
                for (int k = 0; k < 4; k++) {

                    if (ficha[i][j + k] == null) {
                        blanco++;
                    } else if (ficha[i][j + k]) {
                        max++;
                    } else {
                        min++;
                    }
                }
                if (max == 4) {
                    return 1000;
                } else if (min == 4) {
                    return -1000;
                }

                if (max + blanco == 4) {
                    contador++;
                }
                if (min + blanco == 4) {
                    contador--;
                }
            }
        }
        return contador;
    }

    /**
     * Devuelve el número de cuatros en raya que encuentra en las diagonales a
     * derecha
     *
     * @return int contador
     */
    public int diagonalDerecha() {

        int contador = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {

                int max = 0;
                int min = 0;
                int blanco = 0;
                for (int k = 0; k < 4; k++) {

                    if (ficha[i + k][j + k] == null) {
                        blanco++;
                    } else if (ficha[i + k][j + k]) {
                        max++;
                    } else {
                        min++;
                    }
                }
                if (max == 4) {
                    return 1000;
                } else if (min == 4) {
                    return -1000;
                }

                if (max + blanco == 4) {
                    contador++;
                }
                if (min + blanco == 4) {
                    contador--;
                }
            }
        }
        return contador;
    }

    /**
     * Devuelve el número de cuatros en raya que encuentra en las diagonales a
     * izquierda
     *
     * @return int contador
     */
    public int diagonalIzquierda() {

        int contador = 0;
        for (int i = 3; i < DIMX; i++) {
            for (int j = 0; j < 3; j++) {

                int max = 0;
                int min = 0;
                int blanco = 0;
                for (int k = 0; k < 4; k++) {

                    if (ficha[i - k][j + k] == null) {
                        blanco++;
                    } else if (ficha[i - k][j + k]) {
                        max++;
                    } else {
                        min++;
                    }
                }
                if (max == 4) {
                    return 1000;
                } else if (min == 4) {
                    return -1000;
                }

                if (max + blanco == 4) {
                    contador++;
                }
                if (min + blanco == 4) {
                    contador--;
                }
            }
        }
        return contador;
    }

    /**
     * Indica si es un estado final (ganador o empate). Comprobara que hay
     * empate, si todos los huecos estan ocupados.
     *
     * @return {@code true} si es un estado final
     */
    @Override
    public boolean esFinal() {

        int contador = 0;
        for (int i = 0; i < DIMX; i++) {
            contador += getFichas(i);
        }
        if (contador == 6 * 7) {
            return true;
        }

        contador = horizontal();
        if (contador == 1000 || contador == -1000) {
            return true;
        }

        contador = vertical();
        if (contador == 1000 || contador == -1000) {
            return true;
        }

        contador = diagonalDerecha();
        if (contador == 1000 || contador == -1000) {
            return true;
        }

        contador = diagonalIzquierda();
        if (contador == 1000 || contador == -1000) {
            return true;
        }

        return false;
    }

    /**
     * Convierte el estado en una cadena.
     *
     * @return cadena que representa el estado
     */
    @Override
    public String toString() {
        StringBuilder stringBuild = new StringBuilder();
        for (int y = DIMY - 1; y >= 0; y--) {
            stringBuild.append(y + " |");
            for (int x = 0; x < DIMX; x++) {
                Boolean f = ficha[x][y];

                if (f == null) {
                    stringBuild.append(" |");
                } else if (f) {
                    stringBuild.append("X|");
                } else {
                    stringBuild.append("O|");
                }
            }
            stringBuild.append("\n");
        }
        stringBuild.append("   0 1 2 3 4 5 6\n");
        return stringBuild.toString();
    }

} // MiEstado
