package conecta4;

public class Main {

    private static long milisInicio;
    private static int nodosExplorados;// Numero total de nodos explorados

    private static MiEstado maxValor(MiEstado e, int limite) {
        nodosExplorados++;

        if (limite == 0 || e.esFinal()) {
            e.setMinimax(e.getUtilidad());
            return e;
        }

        MiEstado tab = null;

        for (int i = 0; i < 7; i++) {

            MiEstado tab2 = e.getSucesor(i, true);

            if (tab2 != null) {

                MiEstado aux = minValor(tab2, limite - 1);

                if (tab == null || tab.getMinimax() < aux.getMinimax()) {
                    tab = tab2;
                    tab.setMinimax(aux.getMinimax());
                }
            }
        }
        return tab;
    }

    private static MiEstado minValor(MiEstado e, int limite) {
        nodosExplorados++;

        if (limite == 0 || e.esFinal()) {
            e.setMinimax(e.getUtilidad());
            return e;
        }

        MiEstado tab = null;

        for (int i = 0; i < 7; i++) {

            MiEstado tab2 = e.getSucesor(i, false);

            if (tab2 != null) {

                MiEstado aux = maxValor(tab2, limite - 1);

                if (tab == null || tab.getMinimax() > aux.getMinimax()) {
                    tab = tab2;
                    tab.setMinimax(aux.getMinimax());
                }
            }
        }
        return tab;
    }

    public static void main(String args[]) {

        milisInicio = System.currentTimeMillis();

        int limiteMax = 3,
            limiteMin = 7;

        if (args.length != 2) {
            System.out.println("Argumentos:  limiteMin  limiteMax");
        } else {
            limiteMin = Integer.parseInt(args[0]);
            limiteMax = Integer.parseInt(args[1]);
        }

        System.out.println("limiteMin: " + limiteMin);
        System.out.println("limiteMax: " + limiteMax);

        Estado e = new MiEstado();

        while (!e.esFinal()) {

            e = Profesor.maxValor(e, limiteMax);
            e = new MiEstado(e);
            System.out.println(e);

            if (!e.esFinal()) {
                e = minValor((MiEstado) e, limiteMin);
                System.out.println(e);
            }
        }
        long milisTotal = System.currentTimeMillis() - milisInicio;

        System.out.println(" " + nodosExplorados + " nodos explorados");
        System.out.println(" Milisegundos: " + milisTotal);

    }

} // Main
